import { Module } from '@nestjs/common';
import { KafkaProvider } from './kafka.provider';
import { KafkaService } from './kafka.service';

@Module({
  providers: [KafkaProvider, KafkaService],
  exports: [KafkaProvider, KafkaService],
})
export class KafkaModule {}
