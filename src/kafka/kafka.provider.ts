import { Kafka } from 'kafkajs';
import { randomUUID } from 'crypto';

export const KAFKA_CONNECTION = 'KAFKA_CONNECTION';
export const KAFKA_CLIENT_ID = randomUUID();
export const KAFKA_HOSTS = 'kafka:9091';

export const KafkaProvider = {
  provide: KAFKA_CONNECTION,
  useFactory: () => {
    const options = {
      clientId: process.env.KAFKA_CLIENT_ID || KAFKA_CLIENT_ID,
      brokers: (process.env.KAFKA_HOSTS || KAFKA_HOSTS)
        ?.replace(/,\s*$/, '')
        .split(','),
    };
    const kafka = new Kafka(options);
    const producer = kafka.producer();
    return producer;
  },
};
