import { Module } from '@nestjs/common';
import { MongooseProvider } from './database.provider';
import { EventsEntities } from './entities';

@Module({
  providers: [MongooseProvider, ...EventsEntities],
  exports: [MongooseProvider, ...EventsEntities],
})
export class DatabaseModule {}
