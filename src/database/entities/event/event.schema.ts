import * as mongoose from 'mongoose';

export const EventSchema = new mongoose.Schema(
  {
    eventId: String,
    eventName: String,
    eventFromService: String,
    eventDateTime: Date,
    eventData: mongoose.Schema.Types.Mixed,
  },
  { collection: 'event', versionKey: false },
);

export const EVENT = 'Event';
