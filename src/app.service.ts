import { Injectable } from '@nestjs/common';
export const PONG = 'pong';

@Injectable()
export class AppService {
  getPong(): { message: string } {
    return { message: PONG };
  }
}
