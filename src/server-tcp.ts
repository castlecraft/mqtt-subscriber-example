import {
  CustomTransportStrategy,
  MessageHandler,
  ServerTCP,
} from '@nestjs/microservices';

export const WILDCARD_ROUTE = '+';

export class TCPCustomTransport
  extends ServerTCP
  implements CustomTransportStrategy
{
  public getHandlerByPattern(pattern: string): MessageHandler | null {
    const route = this.getRouteFromPattern(pattern);
    return this.messageHandlers.has(route)
      ? this.messageHandlers.get(route)
      : this.messageHandlers.get(WILDCARD_ROUTE);
  }
}
