import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

export const PING = 'ping';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(PING)
  pong(): { message: string } {
    return this.appService.getPong();
  }
}
